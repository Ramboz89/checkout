from flask import request
from functools import wraps


def validate_payload(schema_class, many=False):
    def decorator(f):
        @wraps(f)
        def wrapper(*args, **kw):
            if request.method == 'GET':
                payload = dict(request.args)
            elif request.method in ['POST', 'PUT', 'DELETE']:
                payload = request.get_json(force=True)
            validated_data = schema_class(many=many).load(payload)
            return f(validated_data)
        return wrapper
    return decorator
