from botocore.exceptions import ClientError
from flask import Flask
from flask.logging import create_logger
from marshmallow.exceptions import ValidationError
from blueprints import transaction_bp

app = Flask(__name__)
app.debug = True
create_logger(app)
# Register blueprints ¢∞
app.register_blueprint(transaction_bp)


def build_error_response(source, title, detail, status_code):
    return {
        "errors": [{
            "source": source,
            "title": title,
            "detail": detail
        }]
    }, status_code


# Add custom handling for exceptions
@app.errorhandler(ValidationError)
@app.errorhandler(ClientError)
def handle_error(ex):
    if isinstance(ex, ValidationError):
        return build_error_response(
            ex.messages, "Error validating request payload/params", "Request validation failed", 422
        )
    elif isinstance(ex, ClientError):
        return build_error_response(
            str(ex), "AWS client error", "Client error on aws resource", 500
        )