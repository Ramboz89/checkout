import os

from pynamodb.models import Model


class BaseModel(Model):

    class Meta:
        table_name = os.environ['DYNAMODB_TABLE']
        if 'IS_OFFLINE' in os.environ:
            region = 'localhost'
            host = 'http://localhost:8003'
        else:
            region = os.environ['REGION']
            host = 'https://dynamodb.%s.amazonaws.com' % region
