from uuid import uuid4

from pynamodb.attributes import UnicodeAttribute, NumberAttribute, ListAttribute

from models import BaseModel

DEFAULT_PK = 'TRANSACTION#{}'


class Transaction(BaseModel):

    class Meta(BaseModel.Meta):
        pass

    PK = UnicodeAttribute(hash_key=True, null=False, default=DEFAULT_PK)
    SK = UnicodeAttribute(range_key=True, null=False)
    uuid = UnicodeAttribute(null=False)
    merchant_uuid = UnicodeAttribute(null=True)
    price = NumberAttribute(null=True)
    brand = UnicodeAttribute(null=True)
    supplier = UnicodeAttribute(null=True)
    link = UnicodeAttribute(null=True)
    pictures = ListAttribute(default=[])

    @classmethod
    def find(cls, merchant_uuid, uuid):
        return cls.get(DEFAULT_PK.format(merchant_uuid), 'UUID#{}'.format(uuid))

    @classmethod
    def update_from_dict(cls, data):
        transaction_uuid = data.pop("uuid")
        merchant_uuid = data.pop("merchant_uuid")
        transaction = cls.find(merchant_uuid, transaction_uuid)
        transaction.update(actions=[
            getattr(Transaction, attr_name).set(attr_value) for attr_name, attr_value in data.items()
        ])
        return transaction

    @classmethod
    def create(cls, data):
        transaction_uuid = str(uuid4())
        data["uuid"] = transaction_uuid
        data["SK"] = f"UUID#{transaction_uuid}"
        transaction = cls(**data)
        transaction.save()
        return transaction

    @classmethod
    def all(cls, merchant_uuid, last_evaluated_keys):
        results = cls.query(DEFAULT_PK.format(merchant_uuid), limit=100, last_evaluated_key=last_evaluated_keys)
        transactions = [approved_item for approved_item in results]

        return results.last_evaluated_key, transactions
