import json
import logging
from botocore.exceptions import ClientError
from flask import request, Blueprint
from models import Transaction
from schemas import TransactionSchema
from utils import validate_payload

transaction_bp = Blueprint('transaction', __name__, url_prefix='/transactions')
logger = logging.getLogger()
logger.setLevel(logging.INFO)

@transaction_bp.route("", methods=["POST"])
@validate_payload(TransactionSchema)
def store_transaction(validated_data):
    transaction = Transaction.create(validated_data)
    serialized_transaction = TransactionSchema().dump(transaction)
    return {"data": serialized_transaction}


@transaction_bp.route("", methods=["GET"])
def get_transactions():
    last_evaluated_keys = request.args.get('last_evaluated_key')
    if last_evaluated_keys != "" and last_evaluated_keys is not None:
        last_evaluated_keys = json.loads(last_evaluated_keys)
    # name = request.args.get('name')
    # property_reference = request.args.get('property')
    # account = request.args.get('account')
    last_evaluated_key, transactions = Transaction.all(last_evaluated_keys)
    serialized_transactions = TransactionSchema(many=True).dump(transactions)
    return {
        "data": {
            "transactions": serialized_transactions,
            "last_evaluated_key": json.dumps(last_evaluated_key)
        }
    }