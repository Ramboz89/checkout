.PHONY: local test env install
SHELL := /bin/bash
local:
	source ./venv/bin/activate && serverless wsgi serve --stage local --port 5002

test:
	venv/bin/pytest tests

env:
	rm -rf venv && virtualenv venv -p python38 && source ./venv/bin/activate && pip install -r requirements.txt && pip install -r tests/requirements.txt

install:
	rm -rf node_modules && npm install && serverless dynamodb install