from marshmallow import Schema, fields, post_load, post_dump


class TransactionSchema(Schema):
    """Schema to use when creating creating or displaying approved items
    """
    id = fields.Str(dump_only=True)
    uuid = fields.Str(dump_only=True)
    category_uuid = fields.Str(required=True)
    title = fields.Str(required=True)
    short_description = fields.Str(allow_none=True)
    description = fields.Str(allow_none=True)
    width_cm = fields.Float(allow_none=True)
    depth_cm = fields.Float(allow_none=True)
    height_cm = fields.Float(allow_none=True)
    length_cm = fields.Float(allow_none=True)
    item_weight_grams = fields.Float(allow_none=True)
    colour = fields.Str(allow_none=True)
    warranty = fields.Str(allow_none=True)
    price = fields.Str(required=True)
    brand = fields.Str()
    supplier = fields.Str()
    link = fields.Str()
    pictures = fields.List(fields.Str(), allow_none=True)